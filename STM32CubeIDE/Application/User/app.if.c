/**
 * @file app.if.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Sends periodic messages upstream.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include <lwip/opt.h>
#include <lwip/stats.h>
#include <lwip/debug.h>
#include <lwip/udp.h>

#include "app.acc.h"
#include "app.env.h"
#include "app.fans.h"
#include "app.messages.h"
#include "app.onoff.h"
#include "app.power.h"
#include "app.temp.h"
#include "app.version.h"

static struct udp_pcb* p_send_pcb = NULL;
static uint32_t port = 26000;
static ip4_addr_t dst;
static status_bits_t sb = {.uint_value = 0};

////////////////////////////////////////
// INIT.
void APP_Housekeeper_Init() {
  //  Prep to send UDP packets to the RC.
  p_send_pcb = udp_new();
  IP4_ADDR(&dst, 10, 2, 0, 1);

  // Call init functions for modules that need it.
  ONOFF_Init();
  POW_Init();
  ENV_Init();
  FAN_Init();
  TMP_Init();
  ACC_Init();
}

////////////////////////////////////////
// SEND A MESSAGE.
// Create a pbuf. Put data in it. Send it. Free the buffer.
static void send_message(void* msg, int len) {
  struct pbuf* p = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_POOL);
  if (p == NULL) {
    return;  // Well, we tried.
  }
  pbuf_take(p, msg, len);
  udp_sendto(p_send_pcb, p, &dst, port);
  pbuf_free(p);
}

////////////////////////////////////////
// STATUS.
void APP_Send_Status() {
  extern uint8_t computed_digest[16];
  static m_status_t m_status;
  strncpy(m_status.version, APP_VERSION, 8);
  m_status.opcode = HK_STATUS;
  memcpy(m_status.c_md5, computed_digest, 16);
  m_status.uptime_seconds = HAL_GetTick() / 1000;
  m_status.status_bits = sb.uint_value;

  send_message(&m_status, sizeof(m_status));

  // Clear the status bits now that they've been set.
  sb.uint_value = 0;
}

////////////////////////////////////////
// ACCELEROMETER.
// Default to 1ms until commanded otherwise.
static uint64_t accel_rate_us = 1000;
uint64_t APP_Get_Accel_Rate() { return accel_rate_us; }
void APP_Send_Accel() {
  static m_accel_t m_accel = {
      .opcode = HK_ACCEL,
  };

  m_accel.counter++;
  sb.AccelError |= ACC_read(m_accel.u);

  send_message(&m_accel, sizeof(m_accel));
}

////////////////////////////////////////
// FANS
void APP_Send_Fans() {
  static m_fans_t m_fans = {
      .opcode = HK_FANS,
  };

  sb.FanError |= FAN_read(m_fans.f);

  send_message(&m_fans, sizeof(m_fans));
}

////////////////////////////////////////
// ENV
void APP_Send_Env() {
  static m_env_t m_env = {
      .opcode = HK_ENV,
  };

  sb.EnvSensorError |= ENV_read(&m_env.temp, &m_env.humidity, &m_env.pressure);

  send_message(&m_env, sizeof(m_env));
}

////////////////////////////////////////
// TEMP
void APP_Send_Temp() {
  static m_temp_t m_temp = {
      .opcode = HK_TEMP,
  };

  sb.SurfTempsError |= TMP_read(m_temp.t);

  send_message(&m_temp, sizeof(m_temp));
}

////////////////////////////////////////
// PS
static m_ps_t m_ps = {.opcode = HK_PS,
                      .f = {
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                          -1.0F,
                      }};
void APP_Send_Ps() {
  // Get power data.
  sb.PsError |= POW_read(m_ps.f);

  send_message(&m_ps, sizeof(m_ps));
}
float* APP_Get_Ps() { return m_ps.f; }

////////////////////////////////////////
// ON OFF
void APP_Send_OnOff() {
  static m_on_off_t m_on_off = {
      .opcode = HK_ON_OFF,
      .bit_field = {
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      }};

  sb.ActOnOffError |= ONOFF_get(m_on_off.bit_field);

  send_message(&m_on_off, sizeof(m_on_off));
}

/**
 * @file app.env.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Read environmental sensor.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>

#include <stm32f4xx_hal.h>

#include "app.h"

static bool is_initialized = false;

#define BME280_I2C_ADDR (0x77U << 1)  // sensor I2C address (include RW bit!)
#define BME280_TO 10                  // polled io timeout, ms
#define TP_BASE_ADDR 0x88             // address of temp and pressure cal data
#define H_BASE_ADDR 0xE1              // address of humidity cal data
#define BME280_RAWDATA_ADDR 0xF7      // address of first raw data entry
#define PACK_U16(p) (((p)[0]) | ((uint16_t)((p)[1])) << 8)

static uint16_t dig_T1, dig_T2, dig_T3;
static uint16_t dig_P1, dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
static uint16_t dig_H1, dig_H2, dig_H3, dig_H4, dig_H5, dig_H6;

static int readBosch(uint8_t start_addr, uint8_t buf[], uint16_t n_read) {
  HAL_StatusTypeDef status;
  extern I2C_HandleTypeDef hi2c1;

  // send starting address to read
  buf[0] = start_addr;
  status = HAL_I2C_Initiator_Transmit(&hi2c1, BME280_I2C_ADDR, buf, 1, BME280_TO);
  if (status != HAL_OK) {
    return (false);
  }

  // now read the n values
  status = HAL_I2C_Initiator_Receive(&hi2c1, BME280_I2C_ADDR, buf, n_read, BME280_TO);
  if (status != HAL_OK) {
    return (false);
  }

  return (true);
}

static bool writeBosch(uint8_t buf[], uint16_t n_write) {
  HAL_StatusTypeDef status;
  extern I2C_HandleTypeDef hi2c1;

  status = HAL_I2C_Initiator_Transmit(&hi2c1, BME280_I2C_ADDR, buf, n_write, BME280_TO);
  if (status != HAL_OK) {
    return (false);
  }

  return (true);
}

/**
 * @brief Initialize the devices to read the environmental data.
 *
 */
void ENV_Init() {
  extern I2C_HandleTypeDef hi2c1;
  uint8_t buf[30];

  HAL_StatusTypeDef s = HAL_I2C_IsDeviceReady(&hi2c1, BME280_I2C_ADDR, 3, 3);
  if (s != HAL_OK) {
    return;
  }

  // confirm chip ID is 0x60 to check connection
  if (!readBosch(0xD0, buf, 1)) {
    return;
  }
  if (buf[0] != 0x60) {
    return;
  }

  // perform a full fresh reset
  buf[0] = 0xE0;
  buf[1] = 0xB6;
  if (!writeBosch(buf, 2)) {
    return;
  }
  HAL_Delay(100);  // needed?

  // enable humidity measurements
  buf[0] = 0xF2;
  buf[1] = 0x01;
  if (!writeBosch(buf, 2)) {
    return;
  }

  // enable pressure and temp measurements and use normal mode
  buf[0] = 0xF4;
  buf[1] = 0x27;
  if (!writeBosch(buf, 2)) {
    return;
  }

  // 1s automatic refreshes, filter off
  buf[0] = 0xF5;
  buf[1] = 0xA0;
  if (!writeBosch(buf, 2)) {
    return;
  }

  // read and unpack first chunk of compensation values: temp, pressure and first byte of hum
  if (!readBosch(TP_BASE_ADDR, buf, 26)) {
    return;
  }

  dig_T1 = PACK_U16(&buf[0x88 - TP_BASE_ADDR]);
  dig_T2 = PACK_U16(&buf[0x8A - TP_BASE_ADDR]);
  dig_T3 = PACK_U16(&buf[0x8C - TP_BASE_ADDR]);

  dig_P1 = PACK_U16(&buf[0x8E - TP_BASE_ADDR]);
  dig_P2 = PACK_U16(&buf[0x90 - TP_BASE_ADDR]);
  dig_P3 = PACK_U16(&buf[0x92 - TP_BASE_ADDR]);
  dig_P4 = PACK_U16(&buf[0x94 - TP_BASE_ADDR]);
  dig_P5 = PACK_U16(&buf[0x96 - TP_BASE_ADDR]);
  dig_P6 = PACK_U16(&buf[0x98 - TP_BASE_ADDR]);
  dig_P7 = PACK_U16(&buf[0x9A - TP_BASE_ADDR]);
  dig_P8 = PACK_U16(&buf[0x9C - TP_BASE_ADDR]);
  dig_P9 = PACK_U16(&buf[0x9E - TP_BASE_ADDR]);

  dig_H1 = buf[0xA1 - TP_BASE_ADDR];

  // read and unpack the remaining hum comp values E1 .. E7
  if (!readBosch(H_BASE_ADDR, buf, 7)) {
    return;
  }

  dig_H2 = PACK_U16(&buf[0xE1 - H_BASE_ADDR]);
  dig_H3 = buf[0xE3 - H_BASE_ADDR];
  dig_H4 = (((uint32_t)buf[0xE4 - H_BASE_ADDR]) << 4) | (((uint32_t)buf[0xE5 - H_BASE_ADDR]) & 0xF);
  dig_H5 = (((uint32_t)buf[0xE6 - H_BASE_ADDR]) << 4) | (((uint32_t)buf[0xE5 - H_BASE_ADDR]) >> 4);
  dig_H6 = buf[0xE7 - H_BASE_ADDR];

  is_initialized = true;
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of "5123" equals 51.23 DegC.
// t_fine carries fine temperature as global value
static int32_t t_fine;
static int32_t BME280_compensate_T_int32(int32_t adc_T) {
  int32_t var1, var2, T;
  var1 = ((((adc_T >> 3) - ((int32_t)dig_T1 << 1))) * ((int32_t)dig_T2)) >> 11;
  var2 = (((((adc_T >> 4) - ((int32_t)dig_T1)) * ((adc_T >> 4) - ((int32_t)dig_T1))) >> 12) * ((int32_t)dig_T3)) >> 14;
  t_fine = var1 + var2;
  T = (t_fine * 5 + 128) >> 8;
  return T;
}

// Returns pressure in Pa as unsigned 32 bit integer.
// Output value of "96386" equals 96386 Pa = 963.86 hPa
static uint32_t BME280_compensate_P_int32(int32_t adc_P) {
  int32_t var1, var2;
  uint32_t p;
  var1 = (((int32_t)t_fine) >> 1) - (int32_t)64000;
  var2 = (((var1 >> 2) * (var1 >> 2)) >> 11) * ((int32_t)dig_P6);
  var2 = var2 + ((var1 * ((int32_t)dig_P5)) << 1);
  var2 = (var2 >> 2) + (((int32_t)dig_P4) << 16);
  var1 = (((dig_P3 * (((var1 >> 2) * (var1 >> 2)) >> 13)) >> 3) + ((((int32_t)dig_P2) * var1) >> 1)) >> 18;
  var1 = ((((32768 + var1)) * ((int32_t)dig_P1)) >> 15);
  if (var1 == 0) {
    return 0;  // avoid exception caused by division by zero
  }
  p = (((uint32_t)(((int32_t)1048576) - adc_P) - (var2 >> 12))) * 3125;
  if (p < 0x80000000) {
    p = (p << 1) / ((uint32_t)var1);
  } else {
    p = (p / (uint32_t)var1) * 2;
  }
  var1 = (((int32_t)dig_P9) * ((int32_t)(((p >> 3) * (p >> 3)) >> 13))) >> 12;
  var2 = (((int32_t)(p >> 2)) * ((int32_t)dig_P8)) >> 13;
  p = (uint32_t)((int32_t)p + ((var1 + var2 + dig_P7) >> 4));
  return p;
}

// Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
// Output value of "47445" represents 47445/1024 = 46.333 %RH
static uint32_t BME280_compensate_H_int32(int32_t adc_H) {
  int32_t v_x1_u32r;
  v_x1_u32r = (t_fine - ((int32_t)76800));
  v_x1_u32r =
      (((((adc_H << 14) - (((int32_t)dig_H4) << 20) - (((int32_t)dig_H5) * v_x1_u32r)) + ((int32_t)16384)) >> 15) *
       (((((((v_x1_u32r * ((int32_t)dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) *
             ((int32_t)dig_H2) +
         8192) >>
        14));
  v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((int32_t)dig_H1)) >> 4));
  v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
  v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
  return (uint32_t)(v_x1_u32r >> 12);
}

static void crackRawBuf(uint8_t rawbuf[], float *f, float *h, float *p) {
  // unpack and compensate temperature
  // N.B. do this first because it sets t_fine used by H and P
  int32_t adc_T = ((((uint32_t)rawbuf[0xFA - BME280_RAWDATA_ADDR]) << 12) | (((uint32_t)rawbuf[0xFB - BME280_RAWDATA_ADDR]) << 4) |
                   ((((uint32_t)rawbuf[0xFC - BME280_RAWDATA_ADDR]) >> 4) & 0xF));
  int32_t temp_i = BME280_compensate_T_int32(adc_T);
  *f = temp_i / 100.0F;

  // unpack and compensate humidity
  int32_t adc_H = ((((uint32_t)rawbuf[0xFD - BME280_RAWDATA_ADDR]) << 8) | (((uint32_t)rawbuf[0xFE - BME280_RAWDATA_ADDR]) & 0xFF));
  uint32_t hum_i = BME280_compensate_H_int32(adc_H);
  *h = hum_i / 1024.0F;

  // unpack and compensate pressure
  int32_t adc_P = ((((uint32_t)rawbuf[0xF7 - BME280_RAWDATA_ADDR]) << 12) | (((uint32_t)rawbuf[0xF8 - BME280_RAWDATA_ADDR]) << 4) |
                   ((((uint32_t)rawbuf[0xF9 - BME280_RAWDATA_ADDR]) >> 4) & 0xF));
  int32_t pres_i = BME280_compensate_P_int32(adc_P);
  *p = pres_i / 100.0F;
}

int ENV_read(float *temp, float *humidity, float *pressure) {
  int ret = 0;
  if (!is_initialized) {
    ENV_Init();
  }

  uint8_t buf[8];

  if (readBosch(BME280_RAWDATA_ADDR, buf, 8)) {
    crackRawBuf(buf, temp, humidity, pressure);
  } else {
    ret = 1;
    *temp = -999.0;
    *humidity = -999.0;
    *pressure = -999.0;
  }

  return ret;
}

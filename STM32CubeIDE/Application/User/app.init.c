/**
 * @file app.init.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief CRC calculation.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

uint8_t computed_digest[16];

static void compute_md5();

void APP_Init() {
  compute_md5();
  return;
}

typedef struct {
    char filename[100];
    char mode[8], oid[8], gid[8];
    char size[12];
    char mtime[12];
    char checksum[8];
    char link;
    char linkname[100];
    char ustar[6];
    char version[2];
    char uname[32];
    char gname[32];
    char major[8];
    char minor[8];
    char prefix[155];
    char pad[12];
} ustar_t;

#define SIZE(x) strtol(x->size, NULL, 8)

static void compute_md5() {
	// Compute MD5.
	ustar_t * u = (ustar_t *)(0x08180000);
	int u_len = SIZE(u);
    MD5_CTX ctx;
    MD5Init(&ctx);
    MD5Update(&ctx, u+1, u_len);
    MD5Final(computed_digest, &ctx);
}



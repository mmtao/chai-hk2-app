/**
 * @file app.telnet.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Telnet client.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <lwip/opt.h>
#include <lwip/debug.h>
#include <lwip/stats.h>
#include <lwip/tcp.h>

#include "app.env.h"
#include "app.fans.h"
#include "app.messages.h"
#include "app.messages.on_off.h"
#include "app.onoff.h"
#include "app.version.h"

#include "md5.h"

extern uint32_t APP_expected_crc, APP_calculated_crc, APP_elapsed_milli;

enum trace {
  INIT = 1,
  ACCEPT = 2,
  SEND_STRING = 3,
  RECV = 4,
  POLL = 5,
  SENT = 6,
  SEND = 7,
  ERR = 8,
  CLOSE = 9,
  CHECK = 10,
  SEND_ERR = 11,
};
enum trace trace_buf[256];
uint32_t trace_idx = 0;
#define TRACE(X)            \
  trace_buf[trace_idx] = X; \
  trace_idx = (trace_idx + 1) % 256;

////////////////////////////////////////
// APP Telnet interface.

static char greeting[] = "HK App 2\n", app_version[] = "Version " APP_VERSION "\n", prompt[] = "$ ", green[] = "\033[1;32m", reset[] = "\033[0m",
            newline[] = "\n", red[] = "\033[1;31m";

static char current_command[32];

static struct tcp_pcb *telnet_pcb;

#define STATE_NONE 0
#define STATE_ACCEPTED 1
#define STATE_RECEIVED 2
#define STATE_CLOSING 3

typedef struct {
  struct tcp_pcb *pcb;
  struct pbuf *p;
  uint8_t state, retries;
} telnet_state_t;

// There is only one connection. Behavior with more than one connection
// is undefined and probably very terrible.
static telnet_state_t ts;
static char output_buffer[128];

void APP_verify(struct tcp_pcb *p_pcb);
void APP_telnet_send_string(struct tcp_pcb *p_pcb, char *c);

static void APP_telnet_check_command(struct tcp_pcb *p_pcb) {
  TRACE(CHECK)
  int len = strlen(current_command);

  bool cmd_in_line_mode = current_command[len - 2] == '\r' && current_command[len - 1] == '\n';
  bool cmd_in_char_mode = current_command[len - 1] == '\r';

  if (cmd_in_char_mode) {
    APP_telnet_send_string(p_pcb, newline);
  }

  ////////////////////////////////////////
  // COMMANDS  :
  // "quit"    : End this telnet session.
  // "power"   : Print power supply data.
  // "on_off"  : Show which actuators are on or off.
  // "on x"    : Turn on actuator x.
  // "off x"   : Turn off actuator x.
  // "md5"     : Verify image in flash.
  // "fans"    : Read the value of the fan controller.
  // "fan x y" : Set fan x to setting y.
  // "env"     " Print environmental sensor data.

  if (cmd_in_line_mode || cmd_in_char_mode) {
    // Check for commands we know.
    if (0 == strncmp(current_command, "quit", 4)) {
      //////////
      // QUIT
      ts.state = STATE_CLOSING;
    } else if (0 == strncmp(current_command, "power", 5)) {
      float *APP_Get_Ps();
      float *f = APP_Get_Ps();
      sprintf(output_buffer, "Volts | Amps\n");
      for (int i = 0; i < 14; i++) {
        sprintf(output_buffer + strlen(output_buffer), "%.3f", f[i]);
        if (i % 2 == 0) {
          sprintf(output_buffer + strlen(output_buffer), " | ");
        } else {
          sprintf(output_buffer + strlen(output_buffer), "\n");
        }
      }
      APP_telnet_send_string(p_pcb, output_buffer);
    } else if (0 == strncmp(current_command, "on_off", 6)) {
      uint8_t bit_field[42];
      ONOFF_get(bit_field);
      for (int i = 0; i < 6; i++) {
        output_buffer[0] = 0;
        for (int j = 0; j < 56; j++) {
          sprintf(output_buffer + strlen(output_buffer), "%d", BF_IS_SET(bit_field, (i * 56 + j)));
        }
        sprintf(output_buffer + strlen(output_buffer), "\n");
        APP_telnet_send_string(p_pcb, output_buffer);
      }
    } else if (0 == strncmp(current_command, "on ", 3)) {
      for (int i = 0; i < strlen(current_command); i++) {
        if (current_command[i] == '\r' || current_command[i] == '\n') {
          current_command[i] = 0;
        }
      }
      int n = atoi(current_command + 3);
      uint8_t bit_field[42];
      ONOFF_get(bit_field);
      BF_SET(bit_field, n);
      ONOFF_set(bit_field);
    } else if (0 == strncmp(current_command, "off ", 3)) {
      for (int i = 0; i < strlen(current_command); i++) {
        if (current_command[i] == '\r' || current_command[i] == '\n') {
          current_command[i] = 0;
        }
      }
      int n = atoi(current_command + 3);
      uint8_t bit_field[42];
      ONOFF_get(bit_field);
      BF_UNSET(bit_field, n);
      ONOFF_set(bit_field);
    } else if (0 == strncmp(current_command, "\r\n", 2)) {
    } else if (0 == strncmp(current_command, "md5", 3)) {
      APP_verify(p_pcb);
    } else if (0 == strncmp(current_command, "fans", 4)) {
      float fans[6];
      FAN_read(fans);
      sprintf(output_buffer, "%f %f %f %f %f %f\n", fans[0], fans[1], fans[2], fans[3], fans[4], fans[5]);
      APP_telnet_send_string(p_pcb, output_buffer);
    } else if (0 == strncmp(current_command, "fan ", 4)) {
      int num = atoi(current_command+4);
      float set = atof(current_command+6);
      set = set > 1.0 ? 1.0 : set;
      set = set < 0.0 ? 0.0 : set;
      float fans[6];
      FAN_read(fans);
      fans[num] = set;
      FAN_write(fans);
    } else if (0 == strncmp(current_command, "env", 3)) {
      float t, h, p;
      ENV_read(&t, &h, &p);
      sprintf(output_buffer, "temp=%f, hum=%f, press=%f\n", t, h, p);
      APP_telnet_send_string(p_pcb, output_buffer);
    } else {
      sprintf(output_buffer, "%sUnknown command.%s\n", red, reset);
      APP_telnet_send_string(p_pcb, output_buffer);
    }

    APP_telnet_send_string(p_pcb, prompt);

    memset(current_command, 0, sizeof(current_command));
  }
}

static void APP_telnet_close(struct tcp_pcb *p_pcb, telnet_state_t *p_state) {
  TRACE(CLOSE)
  tcp_arg(p_pcb, NULL);
  tcp_sent(p_pcb, NULL);
  tcp_recv(p_pcb, NULL);
  tcp_err(p_pcb, NULL);
  tcp_poll(p_pcb, NULL, 0);
  tcp_close(p_pcb);

  memset(p_state, 0, sizeof(ts));
}

static void APP_telnet_err(void *arg __attribute__((unused)), err_t err __attribute__((unused))) {
  // Something bad happened. Reset connections state.
  TRACE(ERR)
  memset(&ts, 0, sizeof(ts));
}

static err_t send_err = 0;

static void APP_telnet_send(struct tcp_pcb *p_pcb, telnet_state_t *p_state) {
  TRACE(SEND)
  err_t err = ERR_OK;
  while ((err == ERR_OK) && (p_state->p != NULL) && (p_state->p->len <= tcp_sndbuf(p_pcb))) {
    // Queue data for xmit.
    err = tcp_write(p_pcb, p_state->p->payload, p_state->p->len, TCP_WRITE_FLAG_COPY);
    if (err == ERR_OK) {
      // Remember the current buffer.
      struct pbuf *ptr = p_state->p;
      uint16_t len = ptr->len;

      // Get ready for the next.
      p_state->p = ptr->next;
      if (p_state->p != NULL) {
        // pbuf_ref(p_state->p);
      }

      // Forget the data that's sent.
      pbuf_free(ptr);
      tcp_recved(p_pcb, len);
    } else {
      TRACE(SEND_ERR)
      send_err = err;
    }
  }
}

static err_t APP_telnet_sent(void *arg, struct tcp_pcb *p_pcb, u16_t len) {
  TRACE(SENT)
  telnet_state_t *p_state = (telnet_state_t *)arg;
  p_state->retries = 0;

  if (p_state->p != NULL) {
    // tcp_sent(p_pcb, APP_telnet_sent);
    APP_telnet_send(p_pcb, p_state);
  } else if (p_state->state == STATE_CLOSING) {
    APP_telnet_close(p_pcb, p_state);
  }

  return ERR_OK;
}

static err_t APP_telnet_poll(void *arg, struct tcp_pcb *p_pcb) {
  TRACE(POLL)
  telnet_state_t *p_state = (telnet_state_t *)arg;

  // If there's no state object, abort.
  if (p_state == NULL) {
    tcp_abort(p_pcb);
    return ERR_ABRT;
  }

  // If there is something to process, do that.
  if (p_state->p != NULL) {
    APP_telnet_send(p_pcb, p_state);
  } else if (p_state->state == STATE_CLOSING) {
    APP_telnet_close(p_pcb, p_state);
  }

  return ERR_OK;
}

static err_t APP_telnet_recv(void *arg, struct tcp_pcb *p_pcb, struct pbuf *p, err_t err) {
  TRACE(RECV)
  telnet_state_t *p_state = (telnet_state_t *)arg;

  // Did the remote host close the connection?
  if (p == NULL) {
    p_state->state = STATE_CLOSING;
    if (p_state->p == NULL) {
      APP_telnet_close(p_pcb, p_state);
    } else {
      APP_telnet_send(p_pcb, p_state);
    }
    return ERR_OK;
  }

  // Did some other kind of weird error happen?
  if (err != ERR_OK) {
    pbuf_free(p);
    return err;
  }

  static uint8_t *q;

  // We didn't find errors, so process based on our state.
  switch (p_state->state) {
    case STATE_ACCEPTED:  // Connection established.
      memset(current_command, 0, sizeof(current_command));
      p_state->state = STATE_RECEIVED;
      return ERR_OK;
    case STATE_RECEIVED:  // Now we're getting data.
                          // We might overrun the buffer, and that's not good,
                          // but we aren't going to deal with that here.
      q = (uint8_t *)p->payload;
      if (q[0] != 255) {
        strncat(current_command, p->payload, p->tot_len);
        APP_telnet_check_command(p_pcb);
      }
      tcp_recved(p_pcb, p->tot_len);
      pbuf_free(p);
      return ERR_OK;
    default:
      // We didn't find an error, but our state isn't right to be here. Try to
      // clean up.
      tcp_recved(p_pcb, p->tot_len);
      pbuf_free(p);
      return ERR_OK;
  }

  return ERR_OK;
}

void APP_telnet_send_string(struct tcp_pcb *p_pcb, char *c) {
  TRACE(SEND_STRING)
  // Set up the packet buffer.
  struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, strlen(c), PBUF_RAM);
  pbuf_take(p, c, strlen(c));

  // Queue the packet buffer.
  if (ts.p == NULL) {
    ts.p = p;
    APP_telnet_send(p_pcb, &ts);
  } else {
    pbuf_cat(ts.p, p);
  }
}

// A new connection is accepted.
static err_t APP_telnet_accept(void *arg __attribute__((unused)), struct tcp_pcb *p_pcb, err_t err) {
  TRACE(ACCEPT)
  // Check params.
  if (err != ERR_OK || p_pcb == NULL) {
    return ERR_VAL;
  }

  memset(current_command, 0, sizeof(current_command));

  ts.state = STATE_ACCEPTED;
  ts.pcb = p_pcb;
  ts.retries = 0;
  ts.p = NULL;

  tcp_arg(p_pcb, &ts);
  tcp_recv(p_pcb, APP_telnet_recv);
  tcp_err(p_pcb, APP_telnet_err);
  tcp_poll(p_pcb, APP_telnet_poll, 0);
  tcp_sent(p_pcb, APP_telnet_sent);

  static uint8_t options[] = {255, 251, 34, 0};  // Line mode, please.
  APP_telnet_send_string(p_pcb, (char *)options);

  sprintf(output_buffer, "%s%s%sBooted in %ld ms\n%s%s", green, greeting, app_version, APP_elapsed_milli, reset, prompt);
  APP_telnet_send_string(p_pcb, output_buffer);

  return ERR_OK;
}

void APP_Telnet_Init() {
  TRACE(INIT)
  telnet_pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
  if (telnet_pcb == NULL) {
    return;
  }

  err_t err = tcp_bind(telnet_pcb, IP_ANY_TYPE, 23);
  if (err != ERR_OK) {
    return;
  }

  telnet_pcb = tcp_listen(telnet_pcb);
  tcp_accept(telnet_pcb, APP_telnet_accept);
}

typedef struct {
    char filename[100];
    char mode[8], oid[8], gid[8];
    char size[12];
    char mtime[12];
    char checksum[8];
    char link;
    char linkname[100];
    char ustar[6];
    char version[2];
    char uname[32];
    char gname[32];
    char major[8];
    char minor[8];
    char prefix[155];
    char pad[12];
} ustar_t;

#define SIZE(x) strtol(x->size, NULL, 8)

void APP_verify(struct tcp_pcb *p_pcb) {
	// Compute MD5.

	uint8_t computed_digest[16];
	ustar_t * u = (ustar_t *)(0x08180000);
	int u_len = SIZE(u);

    MD5_CTX ctx;
    MD5Init(&ctx);
    MD5Update(&ctx, u+1, u_len);
    MD5Final(computed_digest, &ctx);

	output_buffer[0] = 0;
    for(int i=0; i<16; i++) {
      sprintf(output_buffer + strlen(output_buffer), "%02x", computed_digest[i]);
    }
    sprintf(output_buffer + strlen(output_buffer), "\n");
    APP_telnet_send_string(p_pcb, output_buffer);

    // Retrieve the expected value.
    int offset = u_len % 512 == 0 ? 0 : 512 - (u_len % 512);
    char * v = (char *) 0x08180000 + 512 + u_len + offset + 512;
    memcpy(output_buffer, v, 32);
    output_buffer[32] = '\n';
    output_buffer[33] = 0;
    APP_telnet_send_string(p_pcb, output_buffer);
}


/**
 * @file app.power.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Get statistics on power consumption from the supplies.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include <stm32f4xx_hal.h>

#include "app.power.h"

/* This code monitors voltage and current from 7 power supplies.
 * 6 INA220 I2C monitors are on I2C-3 switched with a PCA9547 mux for each daughter board.
 * The mainboard monitor is on I2C-1. */

// Addresses, including left shift over R/W bit.
#define INA220_ADDR 0x8A    // power monitor, always the same
#define PCA9547_ADDR 0xE0   // I2C-3 mux
#define INA220_SVR 1        // shunt voltage register
#define INA220_BVR 2        // bus voltage register
#define BVR_SCALE 0.004F    // BVR volts per bit
#define SVR_SCALE 10.0e-6F  // SVR volts per bit
#define MBSHUNT_R 0.002F    // MB shunt resistor, ohms
#define DBSHUNT_R 0.00075F  // DB shunt resistor, ohms (!)
#define TIMEOUT 1           // bus ms

static int read_central(float *v, float *c);
static int read_spoke(int i, float *v, float *c);

void POW_Init() {}

int POW_read(float *f) {
  int ret = 0;

  ret |= read_central(f, f + 1);
  f += 2;

  for (int i = 0; i < 6; i++, f += 2) {
    int temp = read_spoke(i, f, f + 1);
    if (temp != 0) {
      if (f[0] < 4.5 && f[0] > 3.5) {
        // Maybe this is okay. Let it slide, but still set the bits.
        ;
      } else {
        f[0] = -1.0;
        f[1] = -1.0;
      }
    }
    ret |= temp;
  }

  return ret;
}

static int read_central(float *v, float *c) {
  int ret = 0;
  extern I2C_HandleTypeDef hi2c1;

  // Send address of bus voltage register then read MSB followed by LSB.
  uint8_t vbuf[2];
  vbuf[0] = INA220_BVR;
  ret |= (HAL_I2C_Initiator_Transmit(&hi2c1, INA220_ADDR, vbuf, 1, TIMEOUT) == HAL_OK) ? 0 : 1;
  ret |= (HAL_I2C_Initiator_Receive(&hi2c1, INA220_ADDR, vbuf, 2, TIMEOUT) == HAL_OK) ? 0 : 1;
  *v = BVR_SCALE * (((vbuf[0] << 8) | vbuf[1]) >> 3);  // Ignore 3 LSB.

  // Send address of shunt voltage register then read MSB followed by LSB.
  uint8_t ibuf[2];
  ibuf[0] = INA220_SVR;
  ret |= (HAL_I2C_Initiator_Transmit(&hi2c1, INA220_ADDR, ibuf, 1, TIMEOUT) == HAL_OK) ? 0 : 1;
  ret |= (HAL_I2C_Initiator_Receive(&hi2c1, INA220_ADDR, ibuf, 2, TIMEOUT) == HAL_OK) ? 0 : 1;
  uint16_t svr = (ibuf[0] << 8) | ibuf[1];
  svr = (svr & 0x8000U) ? ~svr + 1 : svr;
  *c = SVR_SCALE * svr / MBSHUNT_R;  // I = V/R.

  return ret;
}

static int read_spoke(int i, float *v, float *c) {
  int ret = 0;
  extern I2C_HandleTypeDef hi2c3;

  uint8_t buf[2];

  // Select mux channel.
  buf[0] = 0x08 | (i + 1);  // first DB on SC/D1, not SC/D0
  ret |= (HAL_I2C_Initiator_Transmit(&hi2c3, PCA9547_ADDR, buf, 1, TIMEOUT) == HAL_OK) ? 0 : 1;

  // Send address of bus voltage register then read MSB followed by LSB.
  buf[0] = INA220_BVR;
  ret |= (HAL_I2C_Initiator_Transmit(&hi2c3, INA220_ADDR, buf, 1, TIMEOUT) == HAL_OK) ? 0 : 1;
  ret |= (HAL_I2C_Initiator_Receive(&hi2c3, INA220_ADDR, buf, 2, TIMEOUT) == HAL_OK) ? 0 : 1;
  *v = BVR_SCALE * (((buf[0] << 8) | buf[1]) >> 3);  // Ignore 3 LSB.

  // Send address of Shunt Voltage register then read MSB followed by LSB.
  buf[0] = INA220_SVR;
  ret |= (HAL_I2C_Initiator_Transmit(&hi2c3, INA220_ADDR, buf, 1, TIMEOUT) == HAL_OK) ? 0 : 1;
  ret |= (HAL_I2C_Initiator_Receive(&hi2c3, INA220_ADDR, buf, 2, TIMEOUT) != HAL_OK) ? 0 : 1;
  uint16_t svr = (buf[0] << 8) | buf[1];
  svr = (svr & 0x8000U) ? ~svr + 1 : svr;
  *c = SVR_SCALE * svr / DBSHUNT_R;  // I = V/R.

  return ret;
}

/**
 * @file app.fans.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Fan controller.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <stm32f4xx_hal.h>

#include "app.h"

static bool is_initialized = false;

#define MAX_I2C_ADDR 0x40     // I2C bus address, including left shift over R/W
#define N_FANS 6              // number of fans to control
#define MAX_FAN_RPM 9700.0F   // max fan speed (configure here)
#define PULSES_PER_REV 2      // n tach pulses per fan revolution
#define TACH_PERIODS 2        // tach periods to count for speed, see Fan Dynamisc reg
#define I2C_TIMEOUT 1         // ms
#define TACH_MAX_VAL 2047.0F  // 2^11
#define COUNTER_FREQ 8192.0F  // clock frequency for the tach counter
#define COUNTER_AT_MAX_SPEED COUNTER_FREQ * TACH_PERIODS * 60 / (MAX_FAN_RPM * PULSES_PER_REV)

static float fan_set[] = {0, 0, 0, 0, 0, 0};

static uint8_t write1FanByte(uint8_t reg, uint8_t byte) {
  extern I2C_HandleTypeDef hi2c1;
  uint8_t a[2];
  a[0] = reg;
  a[1] = byte;
  if(HAL_I2C_Initiator_Transmit(&hi2c1, MAX_I2C_ADDR, a, 2, I2C_TIMEOUT) == HAL_OK)
	  return 0;
  else
	  return 1;
}

static uint8_t read1FanByte(uint8_t reg, uint8_t *byte) {
  extern I2C_HandleTypeDef hi2c1;
  uint8_t n_err = 0;
  n_err += HAL_I2C_Initiator_Transmit(&hi2c1, MAX_I2C_ADDR, &reg, 1, I2C_TIMEOUT) != HAL_OK;
  n_err += HAL_I2C_Initiator_Receive(&hi2c1, MAX_I2C_ADDR, byte, 1, I2C_TIMEOUT) != HAL_OK;
  return (n_err);
}

float tach02pwm0(uint16_t tach0) {
  float val = (COUNTER_AT_MAX_SPEED - (float)tach0) / COUNTER_AT_MAX_SPEED;
  if (val > 1.0F) val = 1.0F;
  return val;
}

int FAN_read(float *f) {
  int ret = 0;

  uint8_t n_err = 0;

  // read speeds
  for (uint8_t i = 0; i < N_FANS; i++) {
    uint8_t hibyte, lobyte;
    n_err += read1FanByte(0x18 + 2 * i, &hibyte);  // high 8 bits of 11
    n_err += read1FanByte(0x19 + 2 * i, &lobyte);  // lower 3 bits are in MSB position
    uint16_t tach_count = ((uint16_t)hibyte << 3) | (lobyte >> 5);
    // if tach count is maxed out, this really means fan is stopped
    if (tach_count == 0x7FF) {
      f[i] = 0;
    } else {
      f[i] = tach02pwm0(tach_count);  // 11 bits -> [0,1]
    }
  }

  // FIX ME Just return what we think we did.
  memcpy(f, fan_set, 24);

  // check for any error bits
  uint8_t byte;
  n_err += read1FanByte(0x11, &byte);
  if ((byte & ((2 ^ N_FANS) - 1)) != 0) {  // check all fans
    ret = 1;
  }

  // check for io errors
  if (n_err != 0) {
    for (uint8_t i = 0; i < N_FANS; i++) {
      f[i] = -999.;
    }
    ret = 1;
  }

  return ret;
}

int FAN_write(float *in_f) {
  int ret = 0;

  // set each fan speed
  uint8_t n_err = 0;
  for (uint8_t i = 0; i < N_FANS; i++) {
    float speed_01 = in_f[i];                                     // speed [0,1]
    if (speed_01 >= 0 && speed_01 <= 1) {                             // only process if in range
      uint16_t pwm_count = speed_01 * 511;                            // 9 bits
      n_err += write1FanByte(0x40 + 2 * i, (pwm_count >> 1) & 0xFF);  // upper 8 bits
      n_err += write1FanByte(0x41 + 2 * i, (pwm_count << 7) & 0xFF);  // lower 1 bit in MSB position
    } else {
      ret = 1;
    }
  }

  memcpy(fan_set, in_f, 24);

  // check for io errors
  if (n_err != 0) {
    ret = 1;
  }

  return ret;
}

void FAN_Init() {
  uint8_t n_err = 0;

  // run, normal op, enable bus timeout, use internal oscillator, no I2C watchdog
  n_err += write1FanByte(0x00, 0x00);

  // 25 kHz PWM freq
  n_err += write1FanByte(0x01, 0xBB);

  // PWM, 2s spinup, enable fan control, enable TACH feedback
  for (uint8_t i = 0; i < N_FANS; i++) {
    n_err += write1FanByte(0x02 + i, 0x68);
  }

  // Fan dynamics: count 2 tach periods, change speed 33 to 100% over 2.7 seconds
  for (uint8_t i = 0; i < N_FANS; i++) {
    n_err += write1FanByte(0x08 + i, 0x0C);
  }

  // react to and report faults
  n_err += write1FanByte(0x13, 0x00);

  // no staggered startup, set 0 PWM on fault, require 2 faults
  n_err += write1FanByte(0x14, 0x01);

  // max tach to allow all speeds except stall
  for (uint8_t i = 0; i < N_FANS; i++) {
    n_err += write1FanByte(0x50 + i, 0xFF);
    n_err += write1FanByte(0x51 + i, 0xD0);
  }

  // initially all off (which also prevents false fault)
  for (uint8_t i = 0; i < N_FANS; i++) {
    n_err += write1FanByte(0x40 + 2 * i, 0x00);
    n_err += write1FanByte(0x41 + 2 * i, 0x00);
  }

  // check for io errors
  if (n_err != 0) {
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET); // Turn on the red led.
    return;
  }

  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET); // Make sure the red led is off.
  is_initialized = true;
}

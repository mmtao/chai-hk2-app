/**
 * @file app.acc.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Read accelerometers.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_spi.h>

#include "app.h"

#define NUM_ACCEL_ADC_CHANNELS 16

static uint32_t accel_period_ct;                // sampline period, clock ticks
static uint32_t accel_sample_ct = 0;            // sample counter
static uint8_t adc_rx_data[48];                 // spi dma rx buffer
static uint8_t adc_tx_data[48];                 // spi dma tx buffer, not acutally used... always zero
static int32_t accel_data[16];                  // processed data ready to be read
static uint8_t accel_data_locked;               // lock to ensure data is synchronized
static uint8_t accel_data_valid;                // flag to indicate valid=1/invalid=0
static void accel_cb(SPI_HandleTypeDef* hspi);  // spi callback

void ACC_Init() {
  extern SPI_HandleTypeDef hspi4;
  accel_data_valid = 0;                                                      // data is not valid until successful transfer
  accel_sample_ct = 0;                                                       // sample count
  memset(adc_tx_data, 0, 48);                                                // zero out the tx buffer
  memset(adc_rx_data, 0, 48);                                                // zero out the rx buffer
  HAL_SPI_RegisterCallback(&hspi4, HAL_SPI_TX_RX_COMPLETE_CB_ID, accel_cb);  // called when spi transfer complete
  HAL_GPIO_WritePin(GPIOF, GPIO_PIN_3, 1);                                   // sync ADS1278
}

int ACC_read(uint32_t* u) {
  int ret = 0;
  static int accel_sample_ct_last = -1;
  bool all_ok = (accel_data_valid == 1);
  accel_data_locked = 1;

  if (accel_sample_ct == accel_sample_ct_last) {
    all_ok = false;
  }

  if (all_ok) {
    accel_sample_ct_last = accel_sample_ct;
    memcpy(u, accel_data, sizeof(uint32_t) * 12);
  } else {
    memset(u, 0xff, sizeof(uint32_t) * 12);
    ret = 1;
  }

  return ret;
}

#define MHz 180UL  // system clock rate
int ACC_setperiod(uint32_t usecs) {
  if (usecs >= 500U && usecs < 10000U) {
    accel_period_ct = usecs * MHz;
    return 0;
  } else {
    return 1;
  }
}

/**
 * @brief Called when the SPI data transfer is comlete.
 *
 * Only update if the data is not locked, this is to ensure all data
 * transfered out is from the same instant in time (since all 16
 * channels are snyced). I assume this function cannot be interrupted
 * by readAccel(), so the simple bool lock should surfice. Data from
 * the ADS1278 is 24-bit 2's compliment transmitted MSB.
 *
 * @param hspi
 */
static void accel_cb(SPI_HandleTypeDef* hspi) {
  int itt, pos;
  uint32_t temp_u32;
  if (accel_data_locked == 0) {
    accel_sample_ct++;
    pos = 0;
    for (itt = 0; itt < NUM_ACCEL_ADC_CHANNELS; itt++) {
      temp_u32 = adc_rx_data[pos] << 16;
      pos++;
      temp_u32 += adc_rx_data[pos] << 8;
      pos++;
      temp_u32 += adc_rx_data[pos];
      pos++;

      // convert to 32-bit 2s compliment from 24-bit 2's compliment
      if (temp_u32 & (1 << 23)) {
        accel_data[itt] = temp_u32 | ~((1 << 24) - 1);
      } else {
        accel_data[itt] = temp_u32;
      }
    }
    accel_data_valid = 1;
  }
}

/**
 * @brief Called when an external interrupt is triggered.
 *
 * GPIO_PIN_0 is the "ACC_READY_Pin", which indicates ADS1278 data is ready to
 * transfer. We'll start a DMA SPI transfer and continue on with what ever we were
 * doing. After the DMA transfer completes, the SPI TX/RX complete callback will
 * execute. No data is actually transmitted, but the buffer needs to exist.
 *
 * @param GPIO_Pin
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
  extern SPI_HandleTypeDef hspi4;
  HAL_StatusTypeDef result;
  // start the SPI transfer (using DMA) on the falling edege of ACC_READY_Pin
  if (GPIO_Pin == GPIO_PIN_0) {
    memset(adc_rx_data, 0, 48);
    result = HAL_SPI_TransmitReceive_DMA(&hspi4, adc_tx_data, adc_rx_data, 48);
    if (result != HAL_OK) {
      accel_data_valid = 0;  // flag the data as not valid
    }
  }
}

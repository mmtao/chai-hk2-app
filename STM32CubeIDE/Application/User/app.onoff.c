/**
 * @file hk.onoff.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Turn actuators on and off.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <stm32f4xx_hal.h>

#include "app.onoff.h"
#include "app.messages.on_off.h"

/* ONOFF_Init starts by turning all of the actuators off. They don't turn on or off without
 * us knowing, so we can assume they're off unless we told them to turn on. So, _get can use
 * a cache of on-off values, and the only time we need to talk to the hardware is when we need
 * to set the on-off values.
 *
 * Control the individual Actuator power on/off controls.
 * We use one I2C bus connected to one PCA9547 mux and thence to 2 PCA9698 on each daughter board.
 * The two PCA9698 on each DB are at the same two addresses. Each control up to 40 Actuators, 1-40
 * one the first, 41-56 on the second. By first setting the mux to the correct board, any 1 of the 6
 * daughter boards may be accessed. */

#define NAC 336              // total number of Actuators in wire protocol
#define NDB 6                // number of daughter boards
#define CPD 2                // PCA9698 IO chips per daughter board
#define KPC 5                // control banks per chip
#define TPK 8                // control bits per bank, one bit per Actuator
#define TPC (TPK * KPC)      // bits per chip (40)
#define KPD (KPC * CPD)      // banks per daughter board (10)
#define APD (NAC / NDB)      // Actuator bits per daughter board (56)
#define PCA9547_ADDR 0xE0    // I2C mux address
#define PCA9698_1_ADDR 0x40  // I2C bus address of first controller for Act 1-40
#define PCA9698_2_ADDR 0x42  // I2C bus address of second controller for Act 41-56
#define BPO 4                // bytes per opcode in comm message buffers
#define TIMEOUT 1            // ms
#define REG_IP_BASE 0x00     // PCA9698 base address of input registers
#define REG_OP_BASE 0x08     // PCA9698 base address of output registers
#define REG_PI_BASE 0x10     // PCA9698 base address of polarity invesion registers
#define REG_IOC_BASE 0x18    // PCA9698 base address of io configuration registers
#define REG_MSK_BASE 0x20    // PCA9698 base address of mask interrupt registers
#define REG_OUTCONF 0x28     // PCA9698 OUTCONF register address
#define REG_MODE 0x2A        // PCA9698 Mode register address

    static uint8_t on_off_cache[42];

static int select_db(uint8_t i);
static int send_byte(bool confirm, uint16_t dev_addr, uint8_t reg_addr, uint8_t datum);
static int recv_byte(uint16_t dev_addr, uint8_t reg_addr, uint8_t* datum);

int ONOFF_get(uint8_t* p) {
  memcpy(p, on_off_cache, 42);
  return 0;
}

int ONOFF_set(uint8_t* p) {
  int ret = 0;

  // Set the cache.
  memcpy(on_off_cache, p, 42);

  // Tell the actuators the news.
  uint8_t* q = on_off_cache;
  for (int i = 0; i < 6; i++) {  // Iterate over the boards.
    // Select the right board.
    ret |= select_db(i);

    // Set the banks.
    ret |= send_byte(true, PCA9698_1_ADDR, REG_OP_BASE + 0, *q++);  // (1) -> Chip 1, Bank 0
    ret |= send_byte(true, PCA9698_1_ADDR, REG_OP_BASE + 1, *q++);  // (2) -> Chip 1, Bank 2
    ret |= send_byte(true, PCA9698_1_ADDR, REG_OP_BASE + 2, *q++);  // (3) -> Chip 1, Bank 3
    ret |= send_byte(true, PCA9698_1_ADDR, REG_OP_BASE + 3, *q++);  // (4) -> Chip 1, Bank 4
    ret |= send_byte(true, PCA9698_1_ADDR, REG_OP_BASE + 4, *q++);  // (5) -> Chip 1, Bank 5
    ret |= send_byte(true, PCA9698_2_ADDR, REG_OP_BASE + 0, *q++);  // (6) -> Chip 2, Bank 0
    ret |= send_byte(true, PCA9698_2_ADDR, REG_OP_BASE + 1, *q++);  // (7) -> Chip 2, Bank 1
  }

  return ret;
}

int ONOFF_Init() {
  extern I2C_HandleTypeDef hi2c3;

  int ret = 0;
  ret = (HAL_I2C_IsDeviceReady(&hi2c3, PCA9698_1_ADDR, 3, 3) == HAL_OK) ? 0 : 1;

  //////////
  // 1. Set up communication to all of the boards.
  for (uint8_t i = 0; i < NDB; i++) {
    // Direct mux to this board.
    ret |= select_db(i);

    // Mode: OE-not; outputs change on Stop; no All-Call; no SMBA.
    send_byte(true, PCA9698_1_ADDR, REG_MODE, 0x00);
    send_byte(true, PCA9698_2_ADDR, REG_MODE, 0x00);

    // OutConf: set actuator bit drivers to totem-pole, FPGA control bits to open-drain.
    send_byte(true, PCA9698_1_ADDR, REG_OUTCONF, 0xFF);
    send_byte(true, PCA9698_2_ADDR, REG_OUTCONF, 0x7F);

    // No interrupts.
    for (uint8_t k = 0; k < KPC; k++) {
      send_byte(true, PCA9698_1_ADDR, REG_MSK_BASE + k, 0xFF);
      send_byte(true, PCA9698_2_ADDR, REG_MSK_BASE + k, 0xFF);
    }

    // Init bit output values to all low for Actuator controls, high for FPGA bits.
    for (uint8_t k = 0; k < KPC; k++) {
      send_byte(true, PCA9698_1_ADDR, REG_OP_BASE + k, 0x00);
      send_byte(true, PCA9698_2_ADDR, REG_OP_BASE + k, k < KPC - 1 ? 0x00 : 0xE0);
    }

    // Init bit direction to all outputs.
    for (uint8_t k = 0; k < KPC; k++) {
      send_byte(true, PCA9698_1_ADDR, REG_IOC_BASE + k, 0x00);
      send_byte(true, PCA9698_2_ADDR, REG_IOC_BASE + k, 0x00);
    }
  }

  //////////
  // Mark all of the actuators off in the cache.
  for (int i = 0; i < 336; i++) {
    on_off_cache[336] = 0;
  }

  return 0;
}

static int select_db(uint8_t i) {
  extern I2C_HandleTypeDef hi2c3;
  uint8_t buf[1];
  buf[0] = 0x08 | (i + 1);  // first DB on SC/D1, not SC/D0
  return (HAL_I2C_Initiator_Transmit(&hi2c3, PCA9547_ADDR, buf, 1, 1) == HAL_OK) ? 0 : 1;
}

static int send_byte(bool confirm, uint16_t dev_addr, uint8_t reg_addr, uint8_t datum) {
  extern I2C_HandleTypeDef hi2c3;
  int ret = 0;
  uint8_t a[2] = {reg_addr, datum};

  ret |= (HAL_I2C_Initiator_Transmit(&hi2c3, dev_addr, a, sizeof(a), TIMEOUT) == HAL_OK) ? 0 : 1;

  if (!confirm) {
    return ret;
  }

  uint8_t back = ~datum;
  for (uint8_t try = 0; try < 3; try++) {
    ret |= recv_byte(dev_addr, reg_addr, &back);
    if (back == datum) {
      return ret;  // confirm ok
    }
  }

  return ret;
}

static int recv_byte(uint16_t dev_addr, uint8_t reg_addr, uint8_t* datum) {
  extern I2C_HandleTypeDef hi2c3;
  int ret = 0;
  uint8_t a[1] = {reg_addr};

  // Send desired register address.
  ret |= (HAL_I2C_Initiator_Transmit(&hi2c3, dev_addr, a, 1, 1) == HAL_OK) ? 0 : 1;

  // Read value at that address.
  ret |= (HAL_I2C_Initiator_Receive(&hi2c3, dev_addr, a, 1, TIMEOUT) == HAL_OK) ? 0 : 1;
  *datum = a[0];

  return ret;
}

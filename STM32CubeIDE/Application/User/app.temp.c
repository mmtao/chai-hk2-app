/**
 * @file app.temp.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Read temperatures.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>

#include <stm32f4xx_hal.h>

#include "app.h"

void TMP_Init() {}

// device register to read 16 bit temp
#define AMBIENT_TEMPERATURE_ADDR 0X05
#define SIGN_BIT 0X10

// n sensors = packet length in floats minus 1 for opcode
#define N_SENSORS 7

// sensor i2c addrs
static uint8_t i2c_addrs[N_SENSORS] = {0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E};

// bus timeout, ms
#define TIMEOUT 10

// given the raw 16 bit register AMBIENT_TEMPERATURE_ADDR return temperature in C
float caculate_temp(uint16_t temp_value)  // sic
{
  float temp = 0;
  uint8_t temp_upper = 0, temp_lower = 0;
  temp_upper = (uint8_t)(temp_value >> 8);
  temp_lower = (uint8_t)temp_value;
  if (temp_upper & SIGN_BIT) {
    temp_upper &= 0x0f;
    temp = 256 - (temp_upper * 16 + temp_lower * 0.0625);
    temp *= -1;
  }
  temp_upper &= 0x0f;
  temp = temp_upper * 16 + temp_lower * 0.0625;
  return temp;
}

int TMP_read(uint32_t *t) {
  uint8_t a[3];
  int ret = 0;
  extern I2C_HandleTypeDef hi2c1;

  for (uint8_t i = 0; i < N_SENSORS; i++) {
    // select temperature register
    a[0] = AMBIENT_TEMPERATURE_ADDR;
    ret |= HAL_I2C_Initiator_Transmit(&hi2c1, i2c_addrs[i] << 1, a, 1, TIMEOUT);
    if (ret != HAL_OK) {
      t[i] = 0xFFFFFFFF;
    } else {
      // read two bytes of temperature
      ret = HAL_I2C_Initiator_Receive(&hi2c1, i2c_addrs[i] << 1, a, 2, TIMEOUT);
      if (ret != HAL_OK) {
        t[i] = 0xFFFFFFFF;
      } else {
        uint16_t reg = ((uint16_t)a[0] << 8) | a[1];
        float temp = caculate_temp(reg);
        memcpy(t + i, &temp, sizeof(uint32_t));
      }
    }
  }

  return ret == 0 ? 0 : 1;
}
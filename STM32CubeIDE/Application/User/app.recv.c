/**
 * @file app.recv.c
 * @author Andrew Gardner (agardner@arizona.edu)
 * @brief Receive UDP messages from the RC and do something with them.
 * @version 0.1
 * @date 2022-05-02
 *
 * @copyright Copyright (c) 2022, Arizona Board of Regents on behalf of the
 * University of Arizona. All rights reserved.
 *
 * This file is part of hk2-app.
 * hk2-app is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * hk2-app is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * hk2-app. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <lwip/opt.h>
#include <lwip/debug.h>
#include <lwip/stats.h>
#include <lwip/udp.h>

#include "app.version.h"

#include "app.messages.h"
#include "app.messages.on_off.h"
#include "app.onoff.h"

static struct udp_pcb *p_pcb;

static void recv_from_rc(void *arg __attribute__((unused)), struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {
  // Do something with the message.
  uint32_t opcode = *(uint32_t *)(p->payload);
  switch (opcode) {
    default:
      break;
    case HK_ON_OFF: {
      m_on_off_t *p_msg = (m_on_off_t *)(p->payload);
      ONOFF_set(p_msg->bit_field);
      break;
    }
  }

  // Free the pbuf now that we're done.
  pbuf_free(p);
}

void APP_Recv_Init() {
  p_pcb = udp_new_ip_type(IPADDR_TYPE_ANY);
  if (p_pcb == NULL) {
    return;
  }

  err_t err = udp_bind(p_pcb, IP_ANY_TYPE, 26000);
  if (err == ERR_OK) {
    udp_recv(p_pcb, recv_from_rc, NULL);
  }
}

#pragma once

// 5.0.0 - Booted.
// 5.0.1 - Sends UDP packets upstream.
// 5.0.2 - Talks to power supplies.
// 5.0.3 - Adds md5 support.
// 5.0.4 - Fans.

#define APP_VERSION "5.0.4"

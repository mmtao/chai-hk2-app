#pragma once

#include <stdint.h>

void APP_Init();
void APP_Telnet_Init();
void APP_Housekeeper_Init();
void APP_Recv_Init();

void APP_Send_Status();
void APP_Send_Accel();
void APP_Send_OnOff();
void APP_Send_Fans();
void APP_Send_Env();
void APP_Send_Temp();
void APP_Send_Ps();

uint64_t APP_Get_Accel_Rate();


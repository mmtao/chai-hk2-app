#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "app.messages.h"

/////
// 0 - 335.

typedef struct {
  uint32_t opcode;
  uint8_t bit_field[42];  // 336 divided by 8 is, miraculously, 42.
} m_on_off_t;

#define BF_IS_SET(bf, i) ((bf[(i >> 3)] & (1 << (i & 0x7))) != 0)
#define BF_SET(bf, i) (bf[(i >> 3)] |= (1 << (i & 0x7)))
#define BF_UNSET(bf, i) (bf[(i >> 3)] &= ~(1 << (i & 0x7)))

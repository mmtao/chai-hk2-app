#pragma once

#include <stdint.h>

int ONOFF_Init();
int ONOFF_get(uint8_t * p);
int ONOFF_set(uint8_t * p);
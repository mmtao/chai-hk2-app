#pragma once

#include <stdint.h>

typedef struct {
  uint32_t opcode;
  float f[6];
} m_fans_t;

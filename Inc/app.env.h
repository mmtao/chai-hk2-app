#pragma once

#include <stdint.h>

void ENV_Init();
int ENV_read(float *temp, float *humidity, float *pressure);

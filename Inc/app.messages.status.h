#pragma once

#include <stdint.h>

typedef struct {
  uint32_t opcode;
  char version[8];
  uint8_t c_md5[16];
  uint32_t uptime_seconds;
  uint32_t status_bits;
} m_status_t;

union status_bits {
  uint32_t uint_value;
  struct {
    uint32_t BadOpCode : 1;       // Err unknown command opcode
    uint32_t BadAccelPeriod : 1;  // Err illegal accel rate
    uint32_t EnvSensorError : 1;  // Err reading environmental sensor
    uint32_t AccelError : 1;      // Err reading acceleromters
    uint32_t ActOnOffError : 1;   // Err controlling Actuator power
    uint32_t PsError : 1;         // Err from any power supply
    uint32_t SurfTempsError : 1;  // Err reading any surface temperature device
    uint32_t FanError : 1;        // Err from fan or controller
    uint32_t FPGATimeout : 1;     // FPGA did not signal ready
  };
} __attribute__((packed));
typedef union status_bits status_bits_t;

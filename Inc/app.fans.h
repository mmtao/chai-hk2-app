#pragma once

#include <stdint.h>

void FAN_Init();
int FAN_read(float *f);
int FAN_write(float *f);
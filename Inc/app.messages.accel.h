#pragma once

#include <stdint.h>

typedef struct {
  uint32_t opcode;
  uint32_t counter;
  uint32_t u[12];
} m_accel_t;

typedef struct {
  uint32_t opcode;
  uint32_t period;
} m_accel_period_t;

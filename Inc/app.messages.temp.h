#pragma once

#include <stdint.h>

typedef struct {
  uint32_t opcode;
  uint32_t t[7];
} m_temp_t;

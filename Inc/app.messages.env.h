#pragma once

#include <stdint.h>

typedef struct {
  uint32_t opcode;
  float temp, humidity, pressure;
} m_env_t;

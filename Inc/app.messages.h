#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef enum {
  HK_STATUS = 0,        // m_status_t
  HK_ENV,               // m_env_t
  HK_ON_OFF,            // m_on_off_t
  HK_PS,                // m_ps_t
  HK_ACCEL,             // m_accel_t
  HK_TEMP,              // m_temp_t
  HK_FANS,              // m_fans_t
  HK_SET_ON_OFF,        // m_on_off_t (same_as_report)
  HK_SET_ACCEL_PERIOD,  // m_accel_period_t
  HK_SET_FAN_SPEED,     // m_fans_t (same as report)
} opcode_t;

#include "app.messages.accel.h"
#include "app.messages.env.h"
#include "app.messages.fans.h"
#include "app.messages.on_off.h"
#include "app.messages.ps.h"
#include "app.messages.status.h"
#include "app.messages.temp.h"
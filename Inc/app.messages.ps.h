#pragma once

#include <stdint.h>

typedef struct {
  uint32_t opcode;
  union {
    struct {
      float main_v, main_i, v1, i1, v2, i2, v3, i3, v4, i4, v5, i5, v6, i6;
    };
    float f[14];
  };
} m_ps_t;


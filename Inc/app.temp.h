#pragma once

#include <stdint.h>

void TMP_Init();
int TMP_read(uint32_t *t);

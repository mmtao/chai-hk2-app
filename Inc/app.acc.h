#pragma once

#include <stdint.h>

void ACC_Init();
int ACC_read(uint32_t * u);
int ACC_setperiod(uint32_t usecs);
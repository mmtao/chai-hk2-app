# hk2-app
An application for the HK (STM32F429Z) on the central hub of the MMT/MAPS adaptive secondary mirror. The main communcation interface is the 100 Mb Ethernet connection. LwIP is used. 

## IP.
Assumes that the RC is `10.2.0.1` and that the HK should be `10.2.0.2`.  It is highly recommended that the file `/etc/hosts` on the RC include the line `10.2.0.2 hk`. HK's MAC address is now `6B:65:65:70:65:72`, which is `keeper` in ASCII hex.

## Commands.
A (very, very) minimal shell is available over telnet. `telnet hk` gives you access to the following commands:
* `crc` : Verify the CRC of the running application.
* `power` : Display statistics from the hub and spoke power supplies.
* `on_off` : Display the power state of the actuators.
* `on` and `off` : Turn an individual actuator on/off, e.g. `on 42` or `off 37`.
* `quit` : End your telnet session.

## Reprogramming.
The application cannot reprogram itself. That function is reserved to the BSL. If you establish a telnet or TFTP session with the BSL within 5 seconds of application of power, it won't boot the application. Then you can reprogram the application.

## Abbreviated flash memory map.
The application only knows about where it is stored in flash:

|  Bank | Name      | Block base address | Size   | Use      | Size |
| ------|-----------|--------------------|--------|----------|------|
|  2    | Sector 12 | 0810_0000          |  16 KB | Settings |  16K |
|       | Sector 20 | 0818_0000          | 128 KB | HK App## | 384K |
|       | Sector 21 | 081A_0000          | 128 KB | ######## |
|       | Sector 22 | 081C_0000          | 128 KB | ######## |
|       | Sector 23 | 081E_0000          | 128 KB | Unused

## Interfaces.
These interfaces are tested:
* _Ethernet RMII_. 100 Mb to the RC.
* _I2C channel #1_. 
    * A single [INA220](https://www.ti.com/product/INA220) to monitor power usage on the central board.
* _I2C channel #3_. 
    * A single [PCA9547 I2C mux](https://www.nxp.com/docs/en/data-sheet/PCA9547.pdf) selects which spoke board (numbered 1 and 6) to talk to. Each spoke board's I2C bus contains: 
        * two [PCA9698 for power control](https://www.nxp.com/docs/en/data-sheet/PCA9698.pdf); and,
        * two [INA220](https://www.ti.com/product/INA220) to monitor power usage on that board.

These interfaces are waiting to be tested:
* _I2C channel #1_. 
    * A single [MAXIM 31790](https://www.maximintegrated.com/en/products/interface/signal-integrity/MAX31790.html) to control six fans. 
    * A single [Bosch BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/) to measure environmentals. 
    * Seven [Microchip MCP9808](https://www.microchip.com/en-us/product/MCP9808) temperature sensors.
* _SPI channel #4_.
    * Two [ADS1278] 8-channel ADCs to read twelve accelerometers.

## License.

![](gpl_v3.png)

Copyright (c) 2022, Arizona Board of Regents on behalf of the University of Arizona. All rights reserved.
